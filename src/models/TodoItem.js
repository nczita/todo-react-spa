export class TodoItem {
  constructor({ id, title, deadline }) {
    this.id = id;
    this.title = title;
    this.deadline = deadline;
  }
}
