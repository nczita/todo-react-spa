import { render, screen, waitFor } from '@testing-library/react';
import { rest } from 'msw';
import { setupServer } from 'msw/node';
import App from './App';

const server = setupServer(
  rest.get('http://localhost:3001/todos', (req, res, ctx) => {
    return res(
      ctx.json([
        { id: '5', title: 'Pay taxes', deadline: '2022-06-25T15:19:21+00:00' },
      ])
    );
  })
);

beforeAll(() => server.listen());
afterEach(() => server.resetHandlers());
afterAll(() => server.close());

test('renders todos', async () => {
  render(<App />);
  await waitFor(() => screen.getByText(/Pay taxes/));
  expect(screen.getByText(/Pay taxes/)).toBeInTheDocument();
});
