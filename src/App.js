import './App.css';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import { TodoAppPage } from './pages/TodoAppPage';
import { TodoItemPage } from './pages/TodoItemPage';
import { ErrorPage } from './pages/ErrorPage';

function App() {
  return (
    <Router>
      <Routes>
        <Route path="/error" element={<ErrorPage />} />
        <Route path="/todos/:id" element={<TodoItemPage />} />
        <Route exact path="/" element={<TodoAppPage />} />
      </Routes>
    </Router>
  );
}

export default App;
