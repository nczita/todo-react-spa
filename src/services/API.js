import axios from 'axios';
import adapter from 'axios/lib/adapters/http';
import { TodoItem } from '../models/TodoItem';

axios.defaults.adapter = adapter;

export class API {
  constructor(url) {
    if (url === undefined || url === '') {
      url = process.env.REACT_APP_API_BASE_URL;
    }
    if (url.endsWith('/')) {
      url = url.substr(0, url.length - 1);
    }
    this.url = url;
  }

  withPath(path) {
    if (!path.startsWith('/')) {
      path = '/' + path;
    }
    return `${this.url}${path}`;
  }

  generateAuthToken() {
    return 'Bearer ' + new Date().toISOString();
  }

  async getAllTodoItems() {
    return axios
      .get(this.withPath('/todos'), {
        headers: {
          Authorization: this.generateAuthToken(),
        },
      })
      .then((r) => r.data.map((p) => new TodoItem(p)));
  }

  async getTodoItem(id) {
    return axios
      .get(this.withPath('/todos/' + id), {
        headers: {
          Authorization: this.generateAuthToken(),
        },
      })
      .then((r) => new TodoItem(r.data));
  }
}

export default new API(
  process.env.REACT_APP_API_BASE_URL || 'http://localhost:3001'
);
