import { Pact } from '@pact-foundation/pact';
import { API } from './API';
import { Matchers } from '@pact-foundation/pact';
import { TodoItem } from '../models/TodoItem';
const { eachLike, like, regex } = Matchers;

const mockProvider = new Pact({
  consumer: 'todo-spa',
  provider: process.env.PACT_PROVIDER
    ? process.env.PACT_PROVIDER
    : 'todo-provider',
});

describe('API Pact test', () => {
  beforeAll(() => mockProvider.setup());
  afterEach(() => mockProvider.verify());
  afterAll(() => mockProvider.finalize());

  describe('retrieving a todo item', () => {
    test('Todo Item of ID 10 exists', async () => {
      // Arrange
      const expectedTodoItem = {
        id: '10',
        title: 'Pay taxes',
        deadline: '2022-06-24T15:19:21+00:00',
      };

      // Uncomment to see this fail
      //   const expectedTodoItem = {
      //     not_a_id: '10',
      //     title: 'Pay credits',
      //     deadline: '2022-06-24T15:19:21+00:00',
      //   };

      await mockProvider.addInteraction({
        state: 'a todo item with ID 10 exists',
        uponReceiving: 'a request to get a todo item',
        withRequest: {
          method: 'GET',
          path: '/todos/10',
          headers: {
            Authorization: like('Bearer 2019-01-14T11:34:18.045Z'),
          },
        },
        willRespondWith: {
          status: 200,
          headers: {
            'Content-Type': regex({
              generate: 'application/json; charset=utf-8',
              matcher: 'application/json;?.*',
            }),
          },
          body: like(expectedTodoItem),
        },
      });

      // Act
      const api = new API(mockProvider.mockService.baseUrl);
      const todoItem = await api.getTodoItem('10');

      // Assert - did we get the expected response
      expect(todoItem).toStrictEqual(new TodoItem(expectedTodoItem));
    });

    test('todo item does not exist', async () => {
      // set up Pact interactions
      await mockProvider.addInteraction({
        state: 'a todo item with ID 11 does not exist',
        uponReceiving: 'a request to get a todo item',
        withRequest: {
          method: 'GET',
          path: '/todos/11',
          headers: {
            Authorization: like('Bearer 2019-01-14T11:34:18.045Z'),
          },
        },
        willRespondWith: {
          status: 404,
        },
      });

      const api = new API(mockProvider.mockService.baseUrl);

      // make request to Pact mock server
      await expect(api.getTodoItem('11')).rejects.toThrow(
        'Request failed with status code 404'
      );
    });
  });
  describe('retrieving todo items', () => {
    test('todo items exists', async () => {
      // set up Pact interactions
      const expectedTodoItem = {
        id: '10',
        title: 'Pay taxes',
        deadline: '2022-06-24T15:19:21+00:00',
      };

      await mockProvider.addInteraction({
        state: 'todo item exist',
        uponReceiving: 'a request to get all todo items',
        withRequest: {
          method: 'GET',
          path: '/todos',
          headers: {
            Authorization: like('Bearer 2019-01-14T11:34:18.045Z'),
          },
        },
        willRespondWith: {
          status: 200,
          headers: {
            'Content-Type': regex({
              generate: 'application/json; charset=utf-8',
              matcher: 'application/json;?.*',
            }),
          },
          body: eachLike(expectedTodoItem),
        },
      });

      const api = new API(mockProvider.mockService.baseUrl);

      // make request to Pact mock server
      const todoItems = await api.getAllTodoItems();

      // assert that we got the expected response
      expect(todoItems).toStrictEqual([new TodoItem(expectedTodoItem)]);
    });
  });
});
