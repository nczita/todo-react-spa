import React, { useState, useEffect } from 'react';
import { useNavigate } from "react-router-dom";
import { Container, Button, Box, LinearProgress } from '@mui/material';
import { TodoList } from '../components/TodoList';
import API from '../services/API';

export const TodoAppPage = () => {
    const [isLoading, setIsLoading] = useState(true);
    const [todoItems, setTodoItems] = useState([]);
    const navigate = useNavigate();

    useEffect(() => {
        API.getAllTodoItems().then(items => {
            setTodoItems(items);
            setIsLoading(false);
        }).catch(e => {
            navigate('/error', { error: e.toString() });
        });
    }, [isLoading]);


    return (
        <Container maxWidth="sm">
            <Box textAlign='center'>
                <h1>Todos</h1>
            </Box>
            <br />
            {isLoading && <LinearProgress />}
            {isLoading || <TodoList items={todoItems} />}
            <br />
            <Box textAlign='center'>
                <Button variant="contained" disabled={isLoading}>New</Button>
            </Box>
        </Container>
    )
}