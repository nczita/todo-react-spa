import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { useNavigate } from "react-router-dom";
import { TodoItem } from '../components/TodoItem';
import API from '../services/API';

export const TodoItemPage = () => {
    const { id } = useParams()
    const [todoItem, setTodoItem] = useState({});
    const navigate = useNavigate();

    useEffect(() => {
        API.getTodoItem(id).then(item => {
            setTodoItem(item);
        }).catch(e => {
            console.error("failed to load todo item ", e);
            navigate('/error', { error: e.toString() });
        });
    }, [id])

    return (
        <TodoItem item={todoItem} />
    )
}
