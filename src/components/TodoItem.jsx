import React from 'react';
import { Card, CardContent, Typography, CardActions, Button } from '@mui/material';

export const TodoItem = ({ item }) => {
    return (
        <Card sx={{ minWidth: 275 }} >
            <CardContent>
                <Typography sx={{ fontSize: 14 }} color="text.secondary" gutterBottom>
                    {item.deadline}
                </Typography>
                <Typography variant="h5" component="div">
                    {item.title}
                </Typography>
            </CardContent>
            <CardActions>
                <Button size="small">Edit</Button>
                <Button size="small">Delete</Button>
            </CardActions>
        </Card>
    )
}