import React from 'react';
import { Stack, Container } from '@mui/material';
import { TodoItem } from './TodoItem';

export const TodoList = ({ items }) => {
    const todoItems = items.map(i => <TodoItem item={i} key={i.id} />)
    return (
        <Container maxWidth="sm">
            <Stack spacing={2}>
                {todoItems}
            </Stack>
        </Container>
    )
}