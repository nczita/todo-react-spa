# TODO Single Page Application

## Simple CI/CD flow

1. Run pact tests to generate contract.

```
$ npm run test:pact
```

2. Publish the contract to Pact Broker [1].

```
${PACT_CLI} publish ${PWD}/pacts --consumer-app-version ${GIT_COMMIT} --tag ${GIT_BRANCH}
```

3. Trigger pact verification on provider - webhook send by Pact Broker (see [2]).

4. Use can-i-deploy to check if consumer not violating contract.

```
${PACT_CLI} broker can-i-deploy \
	  --pacticipant todo-spa \
	  --version ${GIT_COMMIT} \
	  --to-environment production \
	  --retry-while-unknown 0 \
	  --retry-interval 10
```

5. Release to production.
6. Tag contract.

```
${PACT_CLI} broker create-version-tag --pacticipant todo-spa --version ${GIT_COMMIT} --tag prod
```

7. Record deployment.

```
${PACT_CLI} broker record-deployment --pacticipant todo-spa --version ${GIT_COMMIT} --environment production
```

- [1] ${PACT_CLI} is described https://github.com/pact-foundation/pact-ruby-cli
- [2] https://docs.pact.io/pact_broker/webhooks

## Use pact stub to demo app

1. Generate pact contract:

```
$ npm run test:pact
```

2. Start frontend:

```
$ npm run start
```

3. Start backend stub

```
$ docker pull pactfoundation/pact-stub-server
$ docker run --name pact-stub-server -t -p 3001:3001 -v "$(pwd)/pacts/:/app/pacts" pactfoundation/pact-stub-server -p 3001 -d pacts --cors

```

4. Kill the docker container (in other console):

```
$ docker stop pact-stub-server
$ docker rm pact-stub-server
```
