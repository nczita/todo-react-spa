PACTICIPANT := "todo-spa"
PACT_CLI="docker run --rm -v ${PWD}:${PWD} -e PACT_BROKER_BASE_URL pactfoundation/pact-cli:latest"
SHELL := /bin/bash

all: pact_test

pact_test: .env
	npm run test:pact

pact_publish: .env
	@echo "BASE_URL: ${PACT_BROKER_BASE_URL}"
	@echo "TOKEN: ${PACT_BROKER_TOKEN}"
	@"${PACT_CLI}" publish ${PWD}/pacts --consumer-app-version ${GIT_COMMIT} --tag ${GIT_BRANCH} --broker-base-url ${PACT_BROKER_BASE_URL}


pact_can_i_deploy: .env
	@"${PACT_CLI}" broker can-i-deploy \
	  --pacticipant ${PACTICIPANT} \
	  --version ${GIT_COMMIT} \
	  --to-environment production \
	  --retry-while-unknown 0 \
	  --retry-interval 10

deploy_app:
	@echo "Deploying to prod"

pact_record_deployment:
	@"${PACT_CLI}" broker record-deployment \
    --pacticipant ${PACTICIPANT} --version ${GIT_COMMIT} \
    --environment production


.env:
	touch .env